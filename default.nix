{ }:
let
  reflex-platform = import ./reflex-platform.nix {};
in reflex-platform.project ({ pkgs, ... }: {
  packages = {
    futrina-launcher = ./.;
  };
  shells = {
    ghc = [
      "futrina-launcher"
    ];
  };
  overrides = import ./overrides.nix { inherit reflex-platform; };
})
