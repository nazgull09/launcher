module Main where

import Control.Exception
import Data.Text (Text, pack, unpack)
import Data.Time
import Futrina.Aeson
import Futrina.API.V1
import Futrina.Crypto
import Futrina.Tools
import Options.Applicative
import System.Environment (lookupEnv)
import System.IO
import Text.Read

import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text.IO as T

data Options = Options {
  optsUrl     :: !Text
, optsCommand :: !Command
}

type ServerUrl = Text
type Password = Text

data Command =
    CommandSignin !Login !(Maybe Password)
  | CommandVerify !Login !(Maybe Token)
  | CommandGetNews
  | CommandGetVersion
  | CommandDownload !FilePath
  | CommandAddNews !Text !FilePath !(Maybe RawPrivKey)

options :: Parser Options
options = Options
  <$> strOption (
       metavar "SERVER_URL"
    <> long "url"
    <> short 'h'
    <> value "https://furtovina.com"
    <> help "Hostname of server to query from"
    )
  <*> subparser (
       command "signin" (info (signinCmd <**> helper) $ progDesc "Get access token from auth server")
    <> command "verify" (info (verifyCmd <**> helper) $ progDesc "Verify access token in auth server")
    <> command "news" (info (pure CommandGetNews <**> helper) $ progDesc "Get news from auth server")
    <> command "version" (info (pure CommandGetVersion <**> helper) $ progDesc "Get current version info")
    <> command "download" (info (downloadCmd <**> helper) $ progDesc "Download last version and check signature")
    <> command "add-news" (info (addNewsCmd <**> helper) $ progDesc "Add new news record to the server")
    )
  where
    signinCmd = CommandSignin
      <$> strArgument (
           metavar "LOGIN"
        <> help "Login (nickname) in the server to signin with."
        )
      <*> (optional . strArgument) (
           metavar "PASSWORD"
        <> help "Password string, you can specify it via FUTRINA_PASSWORD env or by interactive prompt."
        )
    verifyCmd = CommandVerify
      <$> strArgument (
           metavar "LOGIN"
        <> help "Login (nickname) in the server to signin with."
        )
      <*> (optional . strArgument) (
           metavar "TOKEN"
        <> help "Token string, you can specify it via FUTRINA_TOKEN env or by interactive prompt."
        )
    downloadCmd = CommandDownload
      <$> strArgument (
           metavar "OUTPUT_PATH"
        <> help "Filename of output archive downloaded from server"
        )
    addNewsCmd = CommandAddNews
      <$> strArgument (
           metavar "NEWS_TITLE"
        <> help "Title of news."
        )
      <*> strArgument (
           metavar "NEWS_FILE"
        <> help "File with record body (in Haskell Read format) to add to the server."
        )
      <*> (optional . strArgument) (
           metavar "PRIVATE_KEY"
        <> help "Private key string, you can specify it via FUTRINA_PRIVATE_KEY env or by interactive prompt."
        )

main :: IO ()
main = runTools =<< execParser opts
  where
    opts = info (options <**> helper)
      ( fullDesc
     <> progDesc "Execute action with furtovina auth server"
     <> header "furtovina-tools - allows to manually signin/verify or post news and versions to auth server" )

runTools :: Options -> IO ()
runTools Options{..} = case optsCommand of
  CommandSignin login mpass -> do
    pass <- maybe getPassword pure mpass
    mtoken <- futrinaSignin optsUrl login =<< hashPassword pass
    either (fail . unpack) T.putStrLn mtoken
  CommandVerify login mtoken -> do
    token <- maybe getToken pure mtoken
    mres <- futrinaVerify optsUrl login token
    either (fail . unpack) print mres
  CommandGetNews -> do
    pk <- either (fail . unpack) pure $ parsePubKey aerospacePublicKey
    either (fail . unpack) (T.putStrLn . encodeJson) =<< futrinaGetNews optsUrl pk
  CommandGetVersion -> do
    pk <- either (fail . unpack) pure $ parsePubKey aerospacePublicKey
    mres <- futrinaGetVersion optsUrl pk
    either (fail . unpack) (T.putStrLn . encodeJson) mres
  CommandDownload path -> do
    pk <- either (fail . unpack) pure $ parsePubKey aerospacePublicKey
    minfo <- futrinaGetVersion optsUrl pk
    VersionInfo{..} <- either (fail . unpack) pure minfo
    mbs <- futrinaDownload vinfoUrl vinfoHash path
    either (fail . unpack) pure mbs
  CommandAddNews title path mkey -> do
    t <- getCurrentTime
    rkey <- maybe getPrivateKey pure mkey
    cnt <- readFile path
    markup <- either (fail . ("Reading markup: " <>)) pure $ readEither cnt
    let news = NewsItem t title markup
    key <- either (fail . ("Parsing privkey: " <>) . unpack) pure $ parsePrivKey rkey
    either (fail . unpack) pure =<< futrinaAddNews optsUrl news key

getPassword :: IO Password
getPassword = getSecret "FUTRINA_PASSWORD" "Password: "

getToken :: IO Token
getToken = getSecret "FUTRINA_TOKEN" "Token: "

getPrivateKey :: IO RawPrivKey
getPrivateKey = getSecret "FUTRINA_PRIVATE_KEY" "Private key: "

getSecret :: String -> String -> IO Password
getSecret envVar promptPrefix = do
  mval <- lookupEnv envVar
  maybe (promptSecret promptPrefix) (pure . pack) mval

promptSecret :: String -> IO Text
promptSecret prefix = do
  putStr prefix
  hFlush stdout
  pass <- withEcho False getLine
  putChar '\n'
  pure $ pack pass

withEcho :: Bool -> IO a -> IO a
withEcho echo action = do
  old <- hGetEcho stdin
  bracket_ (hSetEcho stdin echo) (hSetEcho stdin old) action
