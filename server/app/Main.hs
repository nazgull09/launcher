module Main where

import Data.Text (Text, pack)
import Control.Exception
import Network.Wai.Handler.Warp
import Network.Wai.Middleware.RequestLogger
import Options.Applicative
import System.IO
import Futrina.Server

import qualified Data.Text.IO as T

data Options = Options {
  optsCommand :: Command
}

type ServerUrl = Text

data Command =
    CommandListen FilePath

options :: Parser Options
options = Options
  <$> subparser (
       command "listen" (info (listenCmd <**> helper) $ progDesc "Start server")
  )
  where
    listenCmd = CommandListen
      <$> strArgument (
          metavar "CONFIG_PATH"
        )

main :: IO ()
main = startServer =<< execParser opts
  where
    opts = info (options <**> helper)
      ( fullDesc
     <> progDesc "Starts Furtovina authorisation server"
     <> header "furtovina-auth-server - an authorisation server for furtovina launcher and server" )

startServer :: Options -> IO ()
startServer Options{..} = case optsCommand of
  CommandListen cfgPath -> do
    cfg <- loadConfig cfgPath
    env <- newEnv cfg
    T.putStrLn $ "Server started at " <> configHost cfg <> ":" <> (pack . show . configPort $ cfg)
    let app = logStdoutDev $ futrinaAuthApp env
        warpSettings = setPort (configPort cfg) defaultSettings
    runSettings warpSettings app
