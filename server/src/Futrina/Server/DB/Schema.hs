module Futrina.Server.DB.Schema where

import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.TH

-- | BCrypt stored hash of password hash that is sent to the server from launcher
type StoredPassHash = Text
-- | Token is random UUID
type Token = Text

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
SignupRec
  login Text
  passHash StoredPassHash
  created UTCTime
  token Text Maybe
  tokenExpire UTCTime Maybe

NewsRec
  title Text
  markup Text
  sign Text
  created UTCTime

VersionRec
  major Int
  minor Int
  patch Int
  meta Text Maybe
  url Text
  hash Text 
  sign Text
  |]
