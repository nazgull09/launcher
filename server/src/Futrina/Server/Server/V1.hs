module Futrina.Server.Server.V1(
    futrinaServer
  ) where

import Futrina.API.V1
import Futrina.Server.DB.Queries
import Futrina.Server.Monad
import Futrina.Server.Password

futrinaServer :: FutrinaApi AsServerM
futrinaServer = FutrinaApi {
    futrinaApiSignin     = signinEndpoint
  , futrinaApiVerify     = verifyEndpoint
  , futrinaApiNews       = getNewsEndpoint
  , futrinaApiAddNews    = addNewsEndpoint
  , futrinaApiVersion    = getVersionEndpoint
  , futrinaApiAddVersion = addVersionEndpoint
  }

signinEndpoint :: SigninReq -> ServerM SigninResp
signinEndpoint (SigninReq login pass) = do
  mp <- runDb $ getUserPassHash login
  let getToken = fmap SigninResp . runDb $ queryUserToken login
  case mp of
    Nothing -> do
      runDb $ registerUser login pass
      getToken
    Just p -> if passwordMatch p pass
      then getToken
      else guardAuthed Nothing

verifyEndpoint :: VerifyReq -> ServerM Bool
verifyEndpoint (VerifyReq login token) = do
  t <- liftIO getCurrentTime
  mexpire <- runDb $ searchToken login token
  pure $ maybe False (t <) mexpire

getNewsEndpoint :: ServerM [(NewsItem, RawSign)]
getNewsEndpoint = runDb getNews

getVersionEndpoint :: ServerM (VersionInfo, RawSign)
getVersionEndpoint = do
  mv <- runDb getLastVersion
  internalError $ maybe (Left "No single version exists!") Right mv

addNewsEndpoint :: (NewsItem, RawSign) -> ServerM ()
addNewsEndpoint (item, s) = do
  guardSign item s
  runDb $ insertNews item s

addVersionEndpoint :: (VersionInfo, RawSign) -> ServerM ()
addVersionEndpoint (vinfo, s) = do
  guardSign vinfo s
  runDb $ insertVersion vinfo s
