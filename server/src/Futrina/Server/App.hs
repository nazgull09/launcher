module Futrina.Server.App(
    futrinaAuthApp
  ) where

import Network.Wai
import Network.Wai.Middleware.Cors
import Network.Wai.Middleware.Gzip
import Servant.API.Generic
import Servant.Server
import Futrina.API
import Futrina.Server.Monad
import Futrina.Server.Server

futrinaAuthApp :: Env -> Application
futrinaAuthApp e = gzip def . appCors $ serve futrinaApi $ hoistServer futrinaApi (runServerM e) $ toServant futrinaAuthServer
  where
    appCors = cors $ const $ Just simpleCorsResourcePolicy
      { corsRequestHeaders = ["Content-Type"]
      , corsMethods = "PUT" : simpleMethods }
