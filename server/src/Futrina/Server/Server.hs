module Futrina.Server.Server(
    futrinaAuthServer
  ) where

import Futrina.API
import Futrina.Server.Monad
import Servant.API.Generic

import qualified Futrina.Server.Server.V1 as V1

futrinaAuthServer :: FutrinaVerApi AsServerM
futrinaAuthServer = FutrinaVerApi {
    futrinaApi'v1  = toServant V1.futrinaServer
  }
