{-# LANGUAGE UndecidableInstances #-}
module Futrina.Launcher.Env(
    MonadLauncher(..)
  , MonadLauncherConstr
  , Env(..)
  , newEnv
  , runEnv
  ) where

import Control.Concurrent
import Control.Monad.Fix
import Control.Monad.Reader
import Data.IORef
import Data.Map.Strict (Map)
import Data.Text (Text)
import Reflex
import Reflex.Dom
import System.IO (Handle)
import System.Process
import Futrina.Launcher.Settings

import qualified Data.Map.Strict as M

type MonadLauncherConstr t m = (MonadHold t m
  , PostBuild t m
  , DomBuilder t m
  , MonadFix m
  , PerformEvent t m
  , MonadIO (Performable m)
  , MonadSample t (Performable m)
  , MonadIO m
  , TriggerEvent t m
  , DomBuilderSpace m ~ GhcjsDomSpace)

class MonadLauncherConstr t m => MonadLauncher t m | m -> t where
  getSettings :: m Settings

data Env = Env {
  env'settings  :: !Settings
}

newEnv :: MonadIO m => Settings -> m Env
newEnv settings = liftIO $ do
  pure Env {
      env'settings = settings
    }

instance MonadLauncherConstr t m => MonadLauncher t (ReaderT Env m) where
  getSettings = asks env'settings
  {-# INLINE getSettings #-}

runEnv :: Env -> ReaderT Env m a -> m a
runEnv = flip runReaderT
