module Futrina.Launcher.Server(
  API(..)
, server
, pAPI
, app
, grabber
) where

import Control.Monad.IO.Class
import Control.Concurrent.Suspend.Lifted
import Control.Concurrent.Timer
import Control.Lens
import Data.Default
import Data.Text
import Data.List (zip4)
import Network.Wai.Handler.Warp
import Network.Wreq hiding (Proxy)
import Servant.API
import Servant.API.Generic
import Servant

import qualified Data.ByteString.Lazy as BL
import qualified Data.Text.Encoding as TE

import Futrina.Launcher.DB
import Futrina.Launcher.Types

type API = "BTC" :> Get '[JSON] [CourseDataTime]
      :<|> "LTC" :> Get '[JSON] [CourseDataTime]
      :<|> "ETH" :> Get '[JSON] [CourseDataTime]
      :<|> "all" :> Get '[JSON] ([CourseDataTime],[CourseDataTime],[CourseDataTime])
      :<|> "allHour" :> Get '[JSON] ([CourseDataTime],[CourseDataTime],[CourseDataTime])
      :<|> "allDay" :> Get '[JSON] ([CourseDataTime],[CourseDataTime],[CourseDataTime])
      :<|> "allWeek" :> Get '[JSON] ([CourseDataTime],[CourseDataTime],[CourseDataTime])
      :<|> "allMonth" :> Get '[JSON] ([CourseDataTime],[CourseDataTime],[CourseDataTime])
      :<|> "test" :> Get '[JSON] [MyInt]

server :: Server API
server = (toHandler (dbselect BTC))
    :<|> (toHandler (dbselect LTC))
    :<|> (toHandler (dbselect ETH))
    :<|> (toHandler (dbselectAll ))
    :<|> (toHandler (dbselectAllHour ))
    :<|> (toHandler (dbselectAllDay ))
    :<|> (toHandler (dbselectAllWeek ))
    :<|> (toHandler (dbselectAllMonth ))
    :<|> (toHandler (dbselectTest))

pAPI :: Proxy API
pAPI = Proxy

app :: Application
app = serve pAPI server

toHandler :: IO a -> Handler a
toHandler a = liftIO a

cryptoCompareKey :: String
cryptoCompareKey = "d4497fa64399f948d958b0203a0ada5c405a29fcc70b0317a1eda6e868a77e28"

getCurrentCourse :: Currency -> IO (Response BL.ByteString)
getCurrentCourse cur = get $ "https://min-api.cryptocompare.com/data/price?fsym=" <> (show cur) <> "&tsyms=USD,EUR,RUR&api_key=" <> cryptoCompareKey

grabbingMinuteHand :: IO (TimerIO)
grabbingMinuteHand = flip repeatedTimer (sDelay 60) $ do
  rBTC <- getCurrentCourse BTC
  case (parseCourseData (rBTC ^. responseBody)) of
    Right cBTC -> do
      dbinsert BTC cBTC
      print $ cBTC
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTC ^. responseBody)))
  rLTC <- getCurrentCourse LTC
  case (parseCourseData (rLTC ^. responseBody)) of
    Right cLTC -> do
      dbinsert LTC cLTC
      print $ cLTC
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTC ^. responseBody)))
  rETH <- getCurrentCourse ETH
  case (parseCourseData (rETH ^. responseBody)) of
    Right cETH -> do
      dbinsert ETH cETH
      print $ cETH
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETH ^. responseBody)))


grabbingHourHand :: IO (TimerIO)
grabbingHourHand = flip repeatedTimer (sDelay 3600) $ do
  rBTC <- getCurrentCourse BTC
  case (parseCourseData (rBTC ^. responseBody)) of
    Right cBTC -> do
      dbinsertHour BTC cBTC
      print $ cBTC
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTC ^. responseBody)))
  rLTC <- getCurrentCourse LTC
  case (parseCourseData (rLTC ^. responseBody)) of
    Right cLTC -> do
      dbinsertHour LTC cLTC
      print $ cLTC
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTC ^. responseBody)))
  rETH <- getCurrentCourse ETH
  case (parseCourseData (rETH ^. responseBody)) of
    Right cETH -> do
      dbinsertHour ETH cETH
      print $ cETH
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETH ^. responseBody)))

grabbingDayHand :: IO (TimerIO)
grabbingDayHand = flip repeatedTimer (sDelay (3600*24)) $ do
  rBTC <- getCurrentCourse BTC
  case (parseCourseData (rBTC ^. responseBody)) of
    Right cBTC -> do
      dbinsertDay BTC cBTC
      print $ cBTC
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTC ^. responseBody)))
  rLTC <- getCurrentCourse LTC
  case (parseCourseData (rLTC ^. responseBody)) of
    Right cLTC -> do
      dbinsertDay LTC cLTC
      print $ cLTC
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTC ^. responseBody)))
  rETH <- getCurrentCourse ETH
  case (parseCourseData (rETH ^. responseBody)) of
    Right cETH -> do
      dbinsertDay ETH cETH
      print $ cETH
    Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETH ^. responseBody)))

grabbingEmptyHand :: IO (TimerIO)
grabbingEmptyHand = flip repeatedTimer (sDelay 5) $ do
  print "test anothor throw"

onceGetHours :: IO ()
onceGetHours = do
    rBTCUSD <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=BTC&tsym=USD&limit=168&api_key=" <> cryptoCompareKey
    rBTCEUR <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=BTC&tsym=EUR&limit=168&api_key=" <> cryptoCompareKey
    rBTCRUR <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=BTC&tsym=RUR&limit=168&api_key=" <> cryptoCompareKey
    case (parseCCHistResponse (rBTCUSD ^. responseBody)) of
      Right cBTCUSD -> do
        case (parseCCHistResponse (rBTCEUR ^. responseBody)) of
            Right cBTCEUR -> do
              case (parseCCHistResponse (rBTCRUR ^. responseBody)) of
                Right cBTCRUR -> do
                  dbinsertHourMultiple BTC btcPrep
                  where
                    btcPrep = fmap (\(usdP,eurP,rurP,timeP) -> CourseDataTime usdP eurP rurP timeP )  $ zip4 (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cBTCUSD))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cBTCEUR))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cBTCRUR))) (fmap ccHistoryData'time (ccMiddleData'Data (ccHistData cBTCUSD)))
                Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTCRUR ^. responseBody)))
            Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTCEUR ^. responseBody)))
      Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTCUSD ^. responseBody)))
    rLTCUSD <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=LTC&tsym=USD&limit=168&api_key=" <> cryptoCompareKey
    rLTCEUR <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=LTC&tsym=EUR&limit=168&api_key=" <> cryptoCompareKey
    rLTCRUR <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=LTC&tsym=RUR&limit=168&api_key=" <> cryptoCompareKey
    case (parseCCHistResponse (rLTCUSD ^. responseBody)) of
      Right cLTCUSD -> do
        case (parseCCHistResponse (rLTCEUR ^. responseBody)) of
            Right cLTCEUR -> do
              case (parseCCHistResponse (rLTCRUR ^. responseBody)) of
                Right cLTCRUR -> do
                  dbinsertHourMultiple LTC ltcPrep
                  where
                    ltcPrep = fmap (\(usdP,eurP,rurP,timeP) -> CourseDataTime usdP eurP rurP timeP )  $ zip4 (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cLTCUSD))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cLTCEUR))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cLTCRUR))) (fmap ccHistoryData'time (ccMiddleData'Data (ccHistData cLTCUSD)))
                Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTCRUR ^. responseBody)))
            Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTCEUR ^. responseBody)))
      Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTCUSD ^. responseBody)))
    rETHUSD <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=ETH&tsym=USD&limit=168&api_key=" <> cryptoCompareKey
    rETHEUR <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=ETH&tsym=EUR&limit=168&api_key=" <> cryptoCompareKey
    rETHRUR <- get $ "https://min-api.cryptocompare.com/data/v2/histohour?fsym=ETH&tsym=RUR&limit=168&api_key=" <> cryptoCompareKey
    case (parseCCHistResponse (rETHUSD ^. responseBody)) of
      Right cETHUSD -> do
        case (parseCCHistResponse (rETHEUR ^. responseBody)) of
            Right cETHEUR -> do
              case (parseCCHistResponse (rETHRUR ^. responseBody)) of
                Right cETHRUR -> do
                  dbinsertHourMultiple ETH ltcPrep
                  where
                    ltcPrep = fmap (\(usdP,eurP,rurP,timeP) -> CourseDataTime usdP eurP rurP timeP )  $ zip4 (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cETHUSD))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cETHEUR))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cETHRUR))) (fmap ccHistoryData'time (ccMiddleData'Data (ccHistData cETHUSD)))
                Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETHRUR ^. responseBody)))
            Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETHEUR ^. responseBody)))
      Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETHUSD ^. responseBody)))



onceGetDays :: IO ()
onceGetDays = do
    rBTCUSD <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=BTC&tsym=USD&limit=365&api_key=" <> cryptoCompareKey
    rBTCEUR <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=BTC&tsym=EUR&limit=365&api_key=" <> cryptoCompareKey
    rBTCRUR <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=BTC&tsym=RUR&limit=365&api_key=" <> cryptoCompareKey
    case (parseCCHistResponse (rBTCUSD ^. responseBody)) of
      Right cBTCUSD -> do
        case (parseCCHistResponse (rBTCEUR ^. responseBody)) of
            Right cBTCEUR -> do
              case (parseCCHistResponse (rBTCRUR ^. responseBody)) of
                Right cBTCRUR -> do
                  dbinsertDayMultiple BTC btcPrep
                  where
                    btcPrep = fmap (\(usdP,eurP,rurP,timeP) -> CourseDataTime usdP eurP rurP timeP )  $ zip4 (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cBTCUSD))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cBTCEUR))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cBTCRUR))) (fmap ccHistoryData'time (ccMiddleData'Data (ccHistData cBTCUSD)))
                Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTCRUR ^. responseBody)))
            Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTCEUR ^. responseBody)))
      Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rBTCUSD ^. responseBody)))
    rLTCUSD <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=LTC&tsym=USD&limit=365&api_key=" <> cryptoCompareKey
    rLTCEUR <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=LTC&tsym=EUR&limit=365&api_key=" <> cryptoCompareKey
    rLTCRUR <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=LTC&tsym=RUR&limit=365&api_key=" <> cryptoCompareKey
    case (parseCCHistResponse (rLTCUSD ^. responseBody)) of
      Right cLTCUSD -> do
        case (parseCCHistResponse (rLTCEUR ^. responseBody)) of
            Right cLTCEUR -> do
              case (parseCCHistResponse (rLTCRUR ^. responseBody)) of
                Right cLTCRUR -> do
                  dbinsertDayMultiple LTC ltcPrep
                  where
                    ltcPrep = fmap (\(usdP,eurP,rurP,timeP) -> CourseDataTime usdP eurP rurP timeP )  $ zip4 (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cLTCUSD))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cLTCEUR))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cLTCRUR))) (fmap ccHistoryData'time (ccMiddleData'Data (ccHistData cLTCUSD)))
                Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTCRUR ^. responseBody)))
            Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTCEUR ^. responseBody)))
      Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rLTCUSD ^. responseBody)))
    rETHUSD <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=ETH&tsym=USD&limit=365&api_key=" <> cryptoCompareKey
    rETHEUR <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=ETH&tsym=EUR&limit=365&api_key=" <> cryptoCompareKey
    rETHRUR <- get $ "https://min-api.cryptocompare.com/data/v2/histoday?fsym=ETH&tsym=RUR&limit=365&api_key=" <> cryptoCompareKey
    case (parseCCHistResponse (rETHUSD ^. responseBody)) of
      Right cETHUSD -> do
        case (parseCCHistResponse (rETHEUR ^. responseBody)) of
            Right cETHEUR -> do
              case (parseCCHistResponse (rETHRUR ^. responseBody)) of
                Right cETHRUR -> do
                  dbinsertDayMultiple ETH ltcPrep
                  where
                    ltcPrep = fmap (\(usdP,eurP,rurP,timeP) -> CourseDataTime usdP eurP rurP timeP )  $ zip4 (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cETHUSD))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cETHEUR))) (fmap ccHistoryData'close (ccMiddleData'Data (ccHistData cETHRUR))) (fmap ccHistoryData'time (ccMiddleData'Data (ccHistData cETHUSD)))
                Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETHRUR ^. responseBody)))
            Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETHEUR ^. responseBody)))
      Left s -> print $ "Error: " <> (pack s) <> " " <> (TE.decodeUtf8 (BL.toStrict (rETHUSD ^. responseBody)))

grabber :: IO ()
grabber = do
  dbcreate
  grabbingMinuteHand
  grabbingHourHand
  grabbingDayHand
  run 8081 app
