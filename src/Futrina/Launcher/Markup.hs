module Futrina.Launcher.Markup(
    Markup(..)
  , renderMarkup
  ) where

import Data.Foldable (traverse_)
import Data.Text (Text)
import Data.Vector (Vector)
import Futrina.Launcher.Aeson
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Reflex.Dom

-- | The simpliest markup language to avoid security problematic markdown
data Markup =
    MText  !Text
  | MSpan  !Text !Markup
  | MPar   !Markup
  | MEnum  !(Vector Markup)
  | MBlock !(Vector Markup)
  deriving (Show)

renderMarkup :: MonadLauncher t m => Markup -> m ()
renderMarkup m = case m of
  MText v -> text v
  MSpan cl v -> spanClass cl $ renderMarkup v
  MPar v -> par $ renderMarkup v
  MEnum vs -> ul $ traverse_ (li . renderMarkup) vs
  MBlock vs -> traverse_ renderMarkup vs

$(deriveJSON aesonOptions ''Markup)
