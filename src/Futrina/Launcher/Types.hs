module Futrina.Launcher.Types(
  currencies
, testResp
, parseCourseData
, parseCCHistResponse
, CourseData(..)
, CourseDataTime(..)
, Currency(..)
, MyInt(..)
, CCHistResponse(..)
, CCHistoryData(..)
, CCMiddleData(..)
) where

import Data.Aeson
import Data.Text
import GHC.Generics

import qualified Data.Text.Encoding as TE
import qualified Data.ByteString.Lazy as BL

data Currency = BTC | LTC | ETH
  deriving (Show, Eq)

currencies :: [Currency]
currencies = [BTC, LTC, ETH]

parseCourseData :: BL.ByteString -> Either String CourseData
parseCourseData cd = eitherDecode cd

parseCCHistResponse :: BL.ByteString -> Either String CCHistResponse
parseCCHistResponse cd = eitherDecode cd

testResp :: Text
testResp = "{\"USD\":8316.74,\"EUR\":7560.23,\"RUR\":540799.69}"


data MyInt = MyInt Int
  deriving (Show, Eq, Generic)

instance ToJSON MyInt where
  toJSON (MyInt a) = object ["value" .= a]

instance FromJSON MyInt where
  parseJSON = withObject "" $ \o -> MyInt
    <$> o .: "value"

data CourseData = CourseData{
  courseData'USD :: !Double
, courseData'EUR :: !Double
, courseData'RUR :: !Double
} deriving (Generic, Show)


instance ToJSON CourseData where
  toJSON CourseData{..} = object [
      "USD" .= courseData'USD
    , "EUR" .= courseData'EUR
    , "RUR" .= courseData'RUR
    ]

instance FromJSON CourseData where
  parseJSON = withObject "" $ \o -> CourseData
    <$> o .: "USD"
    <*> o .: "EUR"
    <*> o .: "RUR"

data CourseDataTime = CourseDataTime{
  courseDataTime'USD  :: !Double
, courseDataTime'EUR  :: !Double
, courseDataTime'RUR  :: !Double
, courseDataTime'time :: !Int
} deriving (Generic, Show)


instance ToJSON CourseDataTime where
  toJSON CourseDataTime{..} = object [
      "USD" .= courseDataTime'USD
    , "EUR" .= courseDataTime'EUR
    , "RUR" .= courseDataTime'RUR
    , "time" .= courseDataTime'time
    ]

instance FromJSON CourseDataTime where
  parseJSON = withObject "" $ \o -> CourseDataTime
    <$> o .: "USD"
    <*> o .: "EUR"
    <*> o .: "RUR"
    <*> o .: "time"


data CCHistResponse = CCHistResponse {
  ccHistResp :: Text
, ccHistData :: CCMiddleData
} deriving (Show, Eq, Generic)

instance ToJSON CCHistResponse where
  toJSON CCHistResponse{..} = object [
      "Response" .= ccHistResp
    , "Data" .= ccHistData
    ]

instance FromJSON CCHistResponse where
  parseJSON = withObject "" $ \o -> CCHistResponse
    <$> o .: "Response"
    <*> o .: "Data"

data CCMiddleData = CCMiddleData {
  ccMiddleData'Aggregated :: Bool
, ccMiddleData'Data :: [CCHistoryData]
} deriving (Show, Eq, Generic)

instance ToJSON CCMiddleData where
  toJSON CCMiddleData{..} = object [
      "Aggregated" .= ccMiddleData'Aggregated
    , "Data" .= ccMiddleData'Data
    ]

instance FromJSON CCMiddleData where
  parseJSON = withObject "" $ \o -> CCMiddleData
    <$> o .: "Aggregated"
    <*> o .: "Data"


data CCHistoryData = CCHistoryData {
  ccHistoryData'close :: Double
, ccHistoryData'time :: Int
} deriving (Show, Eq, Generic)

instance ToJSON CCHistoryData where
  toJSON CCHistoryData{..} = object [
      "close" .= ccHistoryData'close
    , "time" .= ccHistoryData'time
    ]

instance FromJSON CCHistoryData where
  parseJSON = withObject "" $ \o -> CCHistoryData
    <$> o .: "close"
    <*> o .: "time"
