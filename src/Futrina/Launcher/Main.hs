module Futrina.Launcher.Main(
    frontend
  ) where

import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Futrina.Launcher.News
import Reflex.Dom

frontend :: MonadLauncher t m => m ()
frontend = container $ do
  h2 $ text "Aerospace"
  newsWidget
