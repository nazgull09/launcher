module Futrina.Launcher.Settings where

import Control.Lens
import Data.Default
import Data.Maybe
import Data.Text (Text)
import Futrina.Launcher.Aeson
import Futrina.Launcher.Lens

data Settings = Settings {

} deriving (Eq, Show)

instance Default Settings where
  def = Settings

$(deriveJSON (aesonOptionsStripPrefix "settings") ''Settings)

makeLensesWith humbleFields ''Settings
