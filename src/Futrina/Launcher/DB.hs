module Futrina.Launcher.DB(
  dbcreate
, dbinsert
, dbinsertHour
, dbinsertHourMultiple
, dbinsertDay
, dbinsertDayMultiple
, dbinsertTest
, dbselectTest
, dbselect
, dbselectAll
, dbselectAllHour
, dbselectAllDay
, dbselectAllWeek
, dbselectAllMonth
) where

import Data.Monoid
import Data.Text
import Control.Applicative
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow

import Futrina.Launcher.Types

instance ToRow CourseData where
  toRow (CourseData usd eur rur) = toRow (usd, eur, rur)

instance ToRow CourseDataTime where
  toRow (CourseDataTime usd eur rur time) = toRow (usd, eur, rur, time)

instance FromRow CourseDataTime where
  fromRow = CourseDataTime <$> field <*> field <*> field <*> field

instance FromRow MyInt where
  fromRow = MyInt <$> field

dbcreate :: IO ()
dbcreate = do
  conn <- open "test.db"
  execute_ conn $ dbCreateTable BTC
  execute_ conn $ dbCreateTable LTC
  execute_ conn $ dbCreateTable ETH

  execute_ conn $ dbCreateTableHour BTC
  execute_ conn $ dbCreateTableHour LTC
  execute_ conn $ dbCreateTableHour ETH

  execute_ conn $ dbCreateTableDay BTC
  execute_ conn $ dbCreateTableDay LTC
  execute_ conn $ dbCreateTableDay ETH
  close conn

dbinsert :: Currency -> CourseData -> IO ()
dbinsert cur cd = do
  conn <- open "test.db"
  execute conn ("INSERT INTO " <> (showq cur) <> " (usd, eur, rur, time) VALUES (?, ?, ?, strftime('%s','now'))") cd
  close conn

dbinsertHour :: Currency -> CourseData -> IO ()
dbinsertHour cur cd = do
  conn <- open "test.db"
  execute conn ("INSERT INTO " <> (showq cur) <> "hour (usd, eur, rur, time) VALUES (?, ?, ?, strftime('%s','now'))") cd
  close conn

dbinsertDay :: Currency -> CourseData -> IO ()
dbinsertDay cur cd = do
  conn <- open "test.db"
  execute conn ("INSERT INTO " <> (showq cur) <> "day (usd, eur, rur, time) VALUES (?, ?, ?, strftime('%s','now'))") cd
  close conn

dbinsertHourMultiple :: Currency -> [CourseDataTime] -> IO ()
dbinsertHourMultiple cur cdts = do
  conn <- open "test.db"
  traverse (\cdt -> do
     execute conn ("INSERT INTO " <> (showq cur) <> "hour (usd, eur, rur, time) VALUES (?, ?, ?, ?)") cdt) cdts
  close conn

dbinsertDayMultiple :: Currency -> [CourseDataTime] -> IO ()
dbinsertDayMultiple cur cdts = do
  conn <- open "test.db"
  traverse (\cdt -> do
     execute conn ("INSERT INTO " <> (showq cur) <> "day (usd, eur, rur, time) VALUES (?, ?, ?, ?)") cdt) cdts
  close conn
-- ===============================TESTINGS========================================

dbinsertTest :: IO ()
dbinsertTest = do
  conn <- open "test.db"
  execute_ conn ("INSERT INTO inttest (test) VALUES (strftime('%s','now'))")
  close conn

dbselectTest :: IO [MyInt]
dbselectTest = do
  conn <- open "test.db"
  r <- query_ conn $ "SELECT test FROM inttest"
  close conn
  pure r

-- ===============================TESTINGS========================================

dbselect :: Currency -> IO [CourseDataTime]
dbselect cur = do
  conn <- open "test.db"
  r <- query_ conn $ "SELECT usd, eur, rur, time FROM " <> (showq cur)
  close conn
  pure r

dbselectAll :: IO ([CourseDataTime],[CourseDataTime],[CourseDataTime])
dbselectAll = do
  conn <- open "test.db"
  rBTC <- query_ conn $ "SELECT usd, eur, rur, time FROM BTC"
  rLTC <- query_ conn $ "SELECT usd, eur, rur, time FROM LTC"
  rETH <- query_ conn $ "SELECT usd, eur, rur, time FROM ETH"
  close conn
  pure (rBTC, rLTC, rETH)

dbselectAllHour :: IO ([CourseDataTime],[CourseDataTime],[CourseDataTime])
dbselectAllHour = do
  conn <- open "test.db"
  rBTC <- query_ conn $ "SELECT usd, eur, rur, time FROM BTC ORDER BY time DESC LIMIT 60"
  rLTC <- query_ conn $ "SELECT usd, eur, rur, time FROM LTC ORDER BY time DESC LIMIT 60"
  rETH <- query_ conn $ "SELECT usd, eur, rur, time FROM ETH ORDER BY time DESC LIMIT 60"
  close conn
  pure (rBTC, rLTC, rETH)

dbselectAllDay :: IO ([CourseDataTime],[CourseDataTime],[CourseDataTime])
dbselectAllDay = do
  conn <- open "test.db"
  rBTC <- query_ conn $ "SELECT usd, eur, rur, time FROM BTChour ORDER BY time DESC LIMIT 24"
  rLTC <- query_ conn $ "SELECT usd, eur, rur, time FROM LTChour ORDER BY time DESC LIMIT 24"
  rETH <- query_ conn $ "SELECT usd, eur, rur, time FROM ETHhour ORDER BY time DESC LIMIT 24"
  close conn
  pure (rBTC, rLTC, rETH)

dbselectAllWeek :: IO ([CourseDataTime],[CourseDataTime],[CourseDataTime])
dbselectAllWeek = do
  conn <- open "test.db"
  rBTC <- query_ conn $ "SELECT usd, eur, rur, time FROM BTCday ORDER BY time DESC LIMIT 7"
  rLTC <- query_ conn $ "SELECT usd, eur, rur, time FROM LTCday ORDER BY time DESC LIMIT 7"
  rETH <- query_ conn $ "SELECT usd, eur, rur, time FROM ETHday ORDER BY time DESC LIMIT 7"
  close conn
  pure (rBTC, rLTC, rETH)

dbselectAllMonth :: IO ([CourseDataTime],[CourseDataTime],[CourseDataTime])
dbselectAllMonth = do
  conn <- open "test.db"
  rBTC <- query_ conn $ "SELECT usd, eur, rur, time FROM BTCday ORDER BY time DESC LIMIT 30"
  rLTC <- query_ conn $ "SELECT usd, eur, rur, time FROM LTCday ORDER BY time DESC LIMIT 30"
  rETH <- query_ conn $ "SELECT usd, eur, rur, time FROM ETHday ORDER BY time DESC LIMIT 30"
  close conn
  pure (rBTC, rLTC, rETH)

dbCreateTable :: Currency -> Query
dbCreateTable cur = "CREATE TABLE IF NOT EXISTS " <> (showq cur) <> " (id INTEGER PRIMARY KEY, usd REAL, eur REAL, rur REAL, time INTEGER)"

dbCreateTableHour :: Currency -> Query
dbCreateTableHour cur = "CREATE TABLE IF NOT EXISTS " <> (showq cur) <> "hour (id INTEGER PRIMARY KEY, usd REAL, eur REAL, rur REAL, time INTEGER)"

dbCreateTableDay :: Currency -> Query
dbCreateTableDay cur = "CREATE TABLE IF NOT EXISTS " <> (showq cur) <> "day (id INTEGER PRIMARY KEY, usd REAL, eur REAL, rur REAL, time INTEGER)"

showt :: Show a => a -> Text
showt s = pack . show $ s

showq :: Show a => a -> Query
showq s = Query (showt s)
