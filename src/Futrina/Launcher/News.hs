{-# LANGUAGE OverloadedLists #-}
module Futrina.Launcher.News(
    newsWidget
  ) where

import Data.Text (Text)
import Data.Time
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Futrina.Launcher.Markup
import Reflex.Dom

import qualified Data.Text as T

data NewsItem = NewsItem {
  newsItemDate         :: !UTCTime
, newsItemTitle        :: !Text
, newsItemBody         :: !Markup
}

newsWidget :: MonadLauncher t m => m ()
newsWidget = divClass "news" $ renderNewsItem testNews

testDate :: UTCTime
testDate = parseTimeOrError False defaultTimeLocale "%Y-%m-%d %H:%M" "2019-09-17 16:42"

testNews :: NewsItem
testNews = NewsItem {
    newsItemDate = testDate
  , newsItemTitle = "Обнова до 1.4"
  , newsItemBody = MBlock [
      MEnum [
        MText "Добавлен рецепт кварца для хим реактора из слёз гаста"
      , MText "Убран рецепт крафта стали из ICBM"
      ]
    , MPar $ MText "Для инкрементального апдейта закиньте только папку scripts"
    ]
  }

newsDate :: UTCTime -> Text
newsDate = T.pack . formatTime defaultTimeLocale "%Y-%m-%d %H:%M"

renderNewsItem :: MonadLauncher t m => NewsItem -> m ()
renderNewsItem NewsItem{..} = do
  divClass "news-block" $ do
    spanClass "news-title" $ h2 $ text newsItemTitle
    spanClass "news-date" $ text $ newsDate newsItemDate
    renderMarkup newsItemBody
