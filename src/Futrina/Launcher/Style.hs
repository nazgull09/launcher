module Futrina.Launcher.Style(
    frontendCss
  , frontendCssBS
  ) where

import Futrina.Launcher.Style.TH
import Clay
import Clay.Stylesheet
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Text.Lazy.Encoding (encodeUtf8)

frontendCssBS :: ByteString
frontendCssBS = let
  selfcss = toStrict . encodeUtf8 . renderWith compact [] $ frontendCss
  in milligramCss <> tooltipCss <> selfcss

frontendCss :: Css
frontendCss = do
  html ? textAlign center
  body ? do
    color (rgb 179 184 229)
    backgroundColor (rgb 24 41 82)
  ".news" ? do
    width  $ pct 100
    height $ pct 80
    borderRadius (px 10) (px 10) 0 0
    backgroundColor (rgb 59 78 122)
    textAlign $ alignSide sideLeft
    paddingLeft $ px 10
    paddingTop  $ px 15
