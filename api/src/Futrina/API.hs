module Futrina.API(
    FutrinaVerApi(..)
  , futrinaApi
  ) where

import Data.Proxy
import GHC.Generics
import Servant.API
import Servant.API.Generic

import qualified Futrina.API.V1 as V1

-- | Structure with versioned endpoints for Futrina auth server API
data FutrinaVerApi route = FutrinaVerApi
  { futrinaApi'v1      :: route :- "v1" :> ToServant V1.FutrinaApi AsApi
  } deriving (Generic)

-- | Get proxy to the raw API from 'FutrinaApi'
futrinaApi :: Proxy (ToServantApi FutrinaVerApi)
futrinaApi = genericApi (Proxy :: Proxy FutrinaVerApi)
