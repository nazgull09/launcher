module Futrina.API.Key(
    RawPubKey
  , RawSign
  , aerospacePublicKey
  ) where

import Data.Text (Text)

-- | Ed25519 base64 encoded public key
type RawPubKey = Text
-- | Ed25519 base64 encoded signature
type RawSign = Text

-- | Trusted public key, each news record and update archive are signed with the key.
--
-- **MUST not change unless my private key is not stolen.**
aerospacePublicKey :: RawPubKey
aerospacePublicKey = "kGARFsoV08uzlw5WsB5K+cuZasMErRJ6zqsV3zfbs6c="
