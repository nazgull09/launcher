module Futrina.API.V1(
    module Futrina.API.V1
  , module Futrina.API.Key
  , module Futrina.API.Markup
  , module Futrina.API.News
  , module Futrina.API.Version
  ) where

import Data.Text (Text)
import Futrina.Aeson
import Futrina.API.Key
import Futrina.API.Markup
import Futrina.API.News
import Futrina.API.Version
import GHC.Generics
import Servant.API
import Servant.API.Generic

type Login = Text
type PassHash = Text
type Token = Text

-- | Sigin info that is passed to auth server. Password is hashed with blake2
-- before sending (to prevent sniffing on the hacked auth server for reusable
-- passwords).
data SigninReq = SigninReq
  { signinReqLogin    :: !Login
  , signinReqPassHash :: !PassHash
  } deriving (Generic)
$(deriveJSON (aesonOptionsStripPrefix "signinReq") ''SigninReq)

-- | Response that you got from auth server. Contains access token that is passed
-- into game.
data SigninResp = SigninResp
  { signupRespToken :: !Token
  } deriving (Generic)
$(deriveJSON (aesonOptionsStripPrefix "signupResp") ''SigninResp)

-- | Body of verification endpoint for verification that given login actually
-- has the given token.
data VerifyReq = VerifyReq
  { verifyReqLogin :: !Login
  , verifyReqToken :: !Token
  } deriving (Generic)
$(deriveJSON (aesonOptionsStripPrefix "verifyReq") ''VerifyReq)

type Body a = ReqBody '[JSON] a
type Resp a = Post '[JSON] a

-- | Endpoint for signin or automatic signup. If signin is successfull then
-- an access token is returned. The token should be passed into game and server
-- will check it with verify endpoint.
type FutrinaSignin = "signin" :> Body SigninReq :> Resp SigninResp

-- | Checking that the token is still valid for login. Launcher checks the last
-- used token on start and before launch. Server checks client token on login.
type FutrinaVerify = "verify" :> Body VerifyReq :> Resp Bool

-- | Endpoint for getting list of news. Each news item is signed (by serialising it
-- to aeson version of JSON).
type FutrinaGetNews = "news" :> Resp [(NewsItem, RawSign)]

-- | Get current version of game and info where to get client. The version is signed
-- with ed25519 (by serialising it to aeson version of JSON).
type FutrinaGetVersion = "version" :> Resp (VersionInfo, RawSign)

-- | Post new news record
type FutrinaAddNews = "news" :> "add" :> Body (NewsItem, RawSign) :> Resp ()

-- | Post new version and make it current one
type FutrinaAddVersion = "version" :> "publish" :> Body (VersionInfo, RawSign) :> Resp ()

-- | Structure with endpoints for Futrina auth server API
data FutrinaApi route = FutrinaApi
  { futrinaApiSignin     :: route :- FutrinaSignin
  , futrinaApiVerify     :: route :- FutrinaVerify
  , futrinaApiNews       :: route :- FutrinaGetNews
  , futrinaApiVersion    :: route :- FutrinaGetVersion
  , futrinaApiAddNews    :: route :- FutrinaAddNews
  , futrinaApiAddVersion :: route :- FutrinaAddVersion
  } deriving (Generic)
