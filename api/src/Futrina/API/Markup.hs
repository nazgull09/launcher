module Futrina.API.Markup(
    Markup(..)
  ) where

import Data.Aeson 
import Data.Text (Text)
import Data.Vector (Vector)
import Futrina.Aeson

-- | The simpliest markup language to avoid security problematic markdown
data Markup =
    MText  !Text
  | MSpan  !Text !Markup
  | MPar   !Markup
  | MEnum  !(Vector Markup)
  | MBlock !(Vector Markup)
  deriving (Show, Read)

$(deriveJSON aesonOptions {
    sumEncoding = TaggedObject "tag" "cnt"
  } ''Markup)
