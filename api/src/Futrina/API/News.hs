module Futrina.API.News(
    NewsItem(..)
  ) where

import Data.Text (Text)
import Data.Time
import Futrina.Aeson
import Futrina.API.Markup

data NewsItem = NewsItem {
  newsItemDate         :: !UTCTime
, newsItemTitle        :: !Text
, newsItemBody         :: !Markup
} deriving (Show)

$(deriveJSON (aesonOptionsStripPrefix "newsItem") ''NewsItem)
