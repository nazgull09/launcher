module Futrina.API.Version(
    Version(..)
  , VersionInfo(..)
  , showVersion
  ) where

import Data.Text (Text)
import Futrina.Aeson
import Futrina.Text
import GHC.Generics

-- | Semantic version
data Version = Version {
    versionMajor :: !Int -- ^ Breaking changes
  , versionMinor :: !Int -- ^ New features (backward compatible)
  , versionPatch :: !Int -- ^ Bugfixes
  , versionMeta  :: !(Maybe Text) -- ^ Additional metainfo
  } deriving (Generic)
$(deriveJSON (aesonOptionsStripPrefix "version") ''Version)

-- | Show version as major.minor.patch-meta
showVersion :: Version -> Text
showVersion Version{..} = showt versionMajor
                <> "." <> showt versionMinor
                <> "." <> showt versionPatch
                <> (maybe "" ("-" <>) versionMeta)

-- | Version with info where to get the file
data VersionInfo = VersionInfo {
  vinfoVersion :: !Version
, vinfoUrl     :: !Text -- ^ Direct url to download gziped archive
, vinfoHash    :: !Text -- ^ SHA256 hash of archive
}
$(deriveJSON (aesonOptionsStripPrefix "vinfo") ''VersionInfo)
