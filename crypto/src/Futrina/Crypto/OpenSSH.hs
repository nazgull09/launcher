module Futrina.Crypto.OpenSSH(
    parseOpenSSH
  ) where

import Control.Monad
import Crypto.Error (eitherCryptoError, CryptoFailable)
import Data.Bifunctor
import Data.ByteString (ByteString)
import Data.Text (Text, pack)
import Data.Text.Encoding (encodeUtf8)

import qualified Data.Attoparsec.ByteString as A
import qualified Data.ByteString.Base64 as B64

-- | Parse OpenSSH encoded public keys
parseOpenSSH :: (ByteString -> CryptoFailable a) -> Text -> Either Text a
parseOpenSSH f t = do
  bs <- first pack . B64.decode . encodeUtf8 $ t
  v <- first pack . A.parseOnly opensshFormat $ bs
  first (pack . show) . eitherCryptoError . f $ v
  where
    word32 :: A.Parser Int
    word32 = do
      [a, b, c, d] <- fmap fromIntegral <$> replicateM 4 A.anyWord8
      pure $ d + c * 256 + b * 256 * 256 + a * 256 * 256 * 256
    opensshFormat = do
      l1 <- word32
      _ <- A.take l1
      l2 <- word32
      A.take l2
