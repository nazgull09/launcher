module Futrina.Launcher.Markup(
    Markup(..)
  , renderMarkup
  ) where

import Data.Foldable (traverse_)
import Futrina.API.Markup
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Reflex.Dom

renderMarkup :: MonadLauncher t m => Markup -> m ()
renderMarkup m = case m of
  MText v -> text v
  MSpan cl v -> spanClass cl $ renderMarkup v
  MPar v -> par $ renderMarkup v
  MEnum vs -> ul $ traverse_ (li . renderMarkup) vs
  MBlock vs -> traverse_ renderMarkup vs
