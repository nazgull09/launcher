module Futrina.Launcher.Validate(
    validate
  ) where

import Data.Text (Text)
import Futrina.Launcher.Env
import Reflex.Dom

-- | Helper for widget that displays error
errorWidget :: MonadLauncher t m => Text -> m ()
errorWidget = divClass "validate-error" . text

-- | Print in place error message when value is `Left`
validate :: MonadLauncher t m => Event t (Either Text a) -> m (Event t a)
validate e = do
  widgetHold_ (pure ()) $ ffor e $ \case
    Left err -> errorWidget err
    _ -> pure ()
  pure $ fmapMaybe (either (const Nothing) Just) e
