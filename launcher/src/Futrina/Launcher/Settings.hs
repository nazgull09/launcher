module Futrina.Launcher.Settings where

import Control.Lens
import Data.Default
import Data.Text (Text)
import Futrina.Aeson
import Futrina.Launcher.Lens

data Settings = Settings {
  settingsAuthServer :: !Text
} deriving (Eq, Show)

instance Default Settings where
  def = Settings {
      settingsAuthServer = "https://furtovina.com"
    }

$(deriveJSON (aesonOptionsStripPrefix "settings") ''Settings)

makeLensesWith humbleFields ''Settings
