module Futrina.Launcher.Style(
    frontendCss
  , frontendCssBS
  ) where

import Clay
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Text.Lazy.Encoding (encodeUtf8)
import Futrina.Launcher.Style.TH
import Prelude hiding ((**), rem)

frontendCssBS :: ByteString
frontendCssBS = let
  selfcss = toStrict . encodeUtf8 . renderWith compact [] $ frontendCss
  in milligramCss <> tooltipCss <> selfcss

frontendCss :: Css
frontendCss = do
  html ? textAlign center
  body ? do
    color textColor
    backgroundColor majorBackground
  newsCss
  loginFormCss
  errors

textColor :: Color
textColor = rgb 179 184 229

majorBackground :: Color
majorBackground = rgb 24 41 82

minorBackground :: Color
minorBackground = rgb 59 78 122

newsCss :: Css
newsCss = ".news" ? do
  width  $ pct 100
  height $ pct 70
  borderRadius (px 10) (px 10) 0 0
  backgroundColor minorBackground
  textAlign $ alignSide sideLeft
  paddingLeft $ px 10
  paddingTop  $ px 15
  overflowY scroll

loginFormCss :: Css
loginFormCss = do
  form ? do
    marginBottom $ px 0
  fieldset ? do
    marginBottom $ px 0
  ".login-form" ? do
    textAlign $ alignSide sideLeft
    marginTop $ px 15
  ".login-form" ** input ? do
    display inline
    width $ px 150
    marginLeft $ px 15
    color textColor
  ".login-form" ** label ? do
    display inline
    width $ px 150
    marginLeft $ px 15
  ".login-form" ** ".button-primary" ? do
    backgroundColor activeCol
    border solid (rem 0.1) activeCol
    color white
  where
    activeCol = rgb 225 69 148

errors :: Css
errors = do
  ".validate-error" ? do
    display inlineBlock
    marginLeft $ px 20
    fontSize $ pt 15
    color $ rgb 255 10 143
