module Futrina.Launcher.Main(
    frontend
  ) where

import Control.Monad (void)
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Futrina.Launcher.Login
import Futrina.Launcher.News
import Reflex.Dom

frontend :: MonadLauncher t m => m ()
frontend = void $ retractStack mainPage

mainPage :: MonadLauncher t m => m ()
mainPage = container $ do
  h2 $ text "Aerospace"
  newsWidget
  _ <- loginForm
  pure ()
