{-# LANGUAGE UndecidableInstances #-}
module Futrina.Launcher.Env(
    MonadLauncher(..)
  , MonadLauncherConstr
  , Env(..)
  , newEnv
  , runEnv
  , callClient
  , module Reflex.Dom.Retractable.Class
  ) where

import Control.Monad.Fix
import Control.Monad.Reader
import Futrina.Client
import Futrina.Launcher.Settings
import Reflex
import Reflex.Dom
import Reflex.Dom.Retractable.Class
import Reflex.Dom.Retractable.Trans

type MonadLauncherConstr t m = (MonadHold t m
  , PostBuild t m
  , DomBuilder t m
  , MonadFix m
  , PerformEvent t m
  , MonadIO (Performable m)
  , MonadSample t (Performable m)
  , MonadIO m
  , TriggerEvent t m
  , DomBuilderSpace m ~ GhcjsDomSpace
  , MonadRetract t m)

class MonadLauncherConstr t m => MonadLauncher t m | m -> t where
  getSettings :: m Settings
  getManager :: m Manager

data Env = Env {
  env'settings  :: !Settings
, env'manager   :: !Manager
}

newEnv :: MonadIO m => Settings -> m Env
newEnv settings = liftIO $ do
  mng <- createManager
  pure Env {
      env'settings = settings
    , env'manager  = mng
    }

instance MonadLauncherConstr t m => MonadLauncher t (ReaderT Env m) where
  getSettings = asks env'settings
  {-# INLINE getSettings #-}
  getManager = asks env'manager
  {-# INLINE getManager #-}

runEnv :: (Reflex t, TriggerEvent t m) => Env -> ReaderT Env (RetractT t m) a -> m a
runEnv e ma = runRetract (runReaderT ma e)

callClient :: MonadLauncher t m
  => Event t (ClientM a) -- ^ Remote action
  -> m (Event t (Either ServantError a))
callClient mae = do
  Settings{..} <- getSettings
  mng <- getManager
  performEvent $ callFutrina settingsAuthServer mng <$> mae
